package main
 
import (
 "fmt";
 "math";
)
 
func main() {
 //cria um canal com 4 buffers de float64
 c := make(chan float64, 4);
 
 //divide o calculo do PI em 4 partes que podem rodar independente
 go calcular_valor_parcial_pi(1.0, 1000000, c);
 go calcular_valor_parcial_pi(1000001, 2000000, c);
 go calcular_valor_parcial_pi(2000001, 3000000, c);
 go calcular_valor_parcial_pi(3000001, 4000000, c);
 
 //recebe do canal a resposta das 4 execucoes do calculo
 //a ordem que elas foram finalizadas nao importa
 valor_parcial := <-c;
 valor_parcial += <-c;
 valor_parcial += <-c;
 valor_parcial += <-c;
 
 //o valor final do pi se da pelo multiplicacao por 4
 pi := 4.0 * valor_parcial;
 
 //imprime valor de pi calculado na tela
 fmt.Printf("pi = %f n", pi);
}
 
func calcular_valor_parcial_pi(inicio float64, fim float64, c chan float64) {
 var valor float64 = 0.0;
 
 i := inicio;
 
 for i < fim {
 valor += math.Pow(-1.0, i + 1.0) / (2.0 * i - 1.0) ;
 i++;
 }
 
 //coloca no canal qual a resposta do valor parcial a ser calculado
 c <- valor;
}