package main
 
import (
 "http";
 "io";
)
 
func main() {
 //define o endere�o e a fun��o que trata as requisi��es
 http.Handle("/teste_go", http.HandlerFunc(RequestHandler));
 
 //aguarda conex�o na porta 1234
 err := http.ListenAndServe(":1234", nil);
 
 //se aconteceu algum erro
 if err != nil {
 panic("Erro: ", err.String());
 }
}
 
func RequestHandler(c *http.Conn, req *http.Request) {
 //monta uma string com alguns parametros da requisicao
 str :=  "<b>Protocol:</b> " + req.Proto + "<br/>" +
 "<b>Method:</b> " + req.Method + "<br/>" +
 "<b>User-Agent:</b> " + req.UserAgent;
 
 //escreve string como resposta
 io.WriteString(c, str);
}